require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import CompaniesIndex from './components/companies/CompaniesIndex.vue';
import CompaniesCreate from './components/companies/CompaniesCreate.vue';
import CompaniesEdit from './components/companies/CompaniesEdit.vue';

Vue.component('compare', require('./components/companies/CarComparison.vue'));
import CarComparison from './components/companies/CarComparison.vue';


const routes = [{
        path: '/',
        components: {
            companiesIndex: CompaniesIndex
        }
    },
    {
        path: '/admin/companies/create',
        component: CompaniesCreate,
        name: 'createCompany'
    },
    {
        path: '/admin/companies/edit/:id',
        component: CompaniesEdit,
        name: 'editCompany'
    },
    {
        path: '/',
        components: {
            carComparison: CarComparison
        }
    },

]

const router = new VueRouter({
    routes
})

const app = new Vue({
    router
}).$mount('#app')
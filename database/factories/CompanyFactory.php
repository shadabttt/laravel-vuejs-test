<?php

use App\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'id' => $faker->id,
        'name' => $faker->name,
        'price' => $faker->price,
        'fuel_type' => $faker->fuel_type,
        'transmission' => $faker->transmission,
        'gears' => $faker->gears,
        'power_steering' => $faker->power_steering,
        'power_windows' => $faker->power_windows,
        'climate_control' => $faker->climate_control,
        'air_conditioner' => $faker->air_conditioner,
        'fuel_capacity' => $faker->fuel_capacity,
        'seating_capacity' => $faker->seating_capacity,
        'airbag' => $faker->airbag,
    ];
});

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('price')->nullable();;
            $table->string('fuel_type')->nullable();
            $table->string('transmission')->nullable();
            $table->integer('gears')->nullable();
            $table->string('power_steering')->nullable();
            $table->string('power_windows')->nullable();
            $table->string('climate_control')->nullable();
            $table->string('air_conditioner')->nullable();
            $table->integer('fuel_capacity')->nullable();
            $table->integer('seating_capacity')->nullable();
            $table->string('airbag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}


<?php

Route::get('/', function () {
    $cars = DB::table('companies')->get();
    return view('welcome', ['cars' => $cars]);
    // return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('companies', 'CompaniesController@index')->name('companies.index');
});
